package com.mindvalley.android.ui.view

import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.*
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class ChannelsActivityTest {

    companion object {
        private const val PACKAGE = "com.mindvalley.android"
        private const val DEFAULT_TIMEOUT = 20000L
    }

    // Need to make sure app storage is cleared
    @Test
    fun test_noInternetConnection_onFirstTime() {
        val device = UiDevice.getInstance(getInstrumentation())

        //device.executeShellCommand("pm clear com.mindvalley.android")
        device.executeShellCommand("svc wifi disable")
        device.executeShellCommand("svc data disable")

        val scenario = launchActivity<ChannelsActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        val results = device.wait(
            Until.hasObject(By.text("Please check your internet connection and try again.")),
            DEFAULT_TIMEOUT
        )

        assertThat(results).isTrue()
    }

    // Need to make sure wifi is connected and app storage is cleared
    @Test
    fun test_withInternetConnection_onFirstTime() {
        val device = UiDevice.getInstance(getInstrumentation())

        device.executeShellCommand("svc wifi enable")
        device.executeShellCommand("svc data enable")

        val scenario = launchActivity<ChannelsActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        val results = device.wait(
            Until.hasObject(By.clazz(RecyclerView::class.java)),
            DEFAULT_TIMEOUT
        )

        assertThat(results).isTrue()
    }

    // Need to make sure test_withInternetConnection_onFirstTime is successful first
    @Test
    fun test_noInternetConnection_onLocalDataExists() {
        val device = UiDevice.getInstance(getInstrumentation())

        val scenario = launchActivity<ChannelsActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        device.wait(
            Until.hasObject(By.textContains("Showing available offline data.")),
            DEFAULT_TIMEOUT
        )

        val results = device.wait(
            Until.hasObject(By.clazz(RecyclerView::class.java)),
            DEFAULT_TIMEOUT
        )

        assertThat(results).isTrue()
    }

}