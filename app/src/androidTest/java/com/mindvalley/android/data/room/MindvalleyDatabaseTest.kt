package com.mindvalley.android.data.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.mindvalley.android.data.entities.Category
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.data.entities.Media
import com.mindvalley.android.data.room.dao.CategoryDao
import com.mindvalley.android.data.room.dao.ChannelDao
import com.mindvalley.android.data.room.dao.MediaDao
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MindvalleyDatabaseTest {

    private lateinit var database: MindvalleyDatabase
    private lateinit var categoryDao: CategoryDao
    private lateinit var mediaDao: MediaDao
    private lateinit var channelDao: ChannelDao

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, MindvalleyDatabase::class.java).build()

        categoryDao = database.categoryDao()
        mediaDao = database.mediaDao()
        channelDao = database.channelDao()
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    @Test
    fun createReadCategory() = runBlocking {
        val categories = listOf(
            Category("Health & Fitness"),
            Category("Intellectual")
        )
        categoryDao.insertAll(categories)
        val categoriesFromDatabase = categoryDao.getAll()

        val isFirstItemExist = categoriesFromDatabase.find {
            categories[0].name == it.name
        } != null

        val isSecondItemExist = categoriesFromDatabase.find {
            categories[1].name == it.name
        } != null

        assertThat(isFirstItemExist && isSecondItemExist).isTrue()
    }

    @Test
    fun createReadMedia() = runBlocking {
        val medias = listOf(
            Media(null, "The Cure For Loneliness", null, null),
            Media(null, "Evolved Enterprise", null, null)
        )
        mediaDao.insertAll(medias)
        val mediasFromDatabase = mediaDao.getAll()

        val isFirstItemExist = mediasFromDatabase.find {
            medias[0].title == it.title
        } != null

        val isSecondItemExist = mediasFromDatabase.find {
            medias[1].title == it.title
        } != null

        assertThat(isFirstItemExist && isSecondItemExist).isTrue()
    }

    @Test
    fun createReadChannel() = runBlocking {
        val channels = listOf(
            Channel("Mindvalley Mentoring", 150, null, null, null, null),
            Channel("Mindvalley Films", 32, null, null, null, null)
        )
        channelDao.insertAll(channels)
        val channelsFromDatabase = channelDao.getAll()

        val isFirstItemExist = channelsFromDatabase.find {
            channels[0].title == it.title
        } != null

        val isSecondItemExist = channelsFromDatabase.find {
            channels[1].title == it.title
        } != null

        assertThat(isFirstItemExist && isSecondItemExist).isTrue()
    }
}