package com.mindvalley.android.util

import android.content.res.Resources

val Int.dp: Int
    get() = (toFloat() * Resources.getSystem().displayMetrics.density + 0.5f).toInt()