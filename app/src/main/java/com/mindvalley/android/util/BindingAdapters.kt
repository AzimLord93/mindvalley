package com.mindvalley.android.util

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.mindvalley.android.R


object BindingAdapters {

    @JvmStatic
    @BindingAdapter(
        "srcUrl",
        "placeholder",
        requireAll = false
    )
    fun ImageView.bindSrcUrl(
        url: String?,
        placeholder: Drawable?,
    ) = Glide.with(this).load(url).let { request ->
        if (placeholder != null) {
            request.placeholder(placeholder)
        }
        request.into(this)
    }

    @JvmStatic
    @BindingAdapter(
        "isLoading"
    )
    fun SwipeRefreshLayout.bindIsLoading(
        isLoading: Boolean?,
    ) {
        isLoading?.let {
            this.isRefreshing = it
        } ?: run {
            this.isRefreshing = false
        }
    }

    @JvmStatic
    @BindingAdapter(
        "haveDivider",
        "dividerInset",
        requireAll = false
    )
    fun RecyclerView.bindHasDivider(
        haveDivider: Boolean?,
        dividerInset: Int?
    ) {
        if (haveDivider == true) {
            val itemDecoration =
                MaterialDividerItemDecoration(this.context, MaterialDividerItemDecoration.VERTICAL)
            dividerInset?.let {
                itemDecoration.dividerInsetStart = it.dp
                itemDecoration.dividerInsetEnd = it.dp
            }
            itemDecoration.isLastItemDecorated = false
            itemDecoration.dividerColor = ContextCompat.getColor(this.context, R.color.divider)
            this.addItemDecoration(itemDecoration)
        }
    }

    @JvmStatic
    @BindingAdapter(
        "showView"
    )
    fun View.bindShowView(
        showView: Boolean?
    ) {
        alpha = if (showView == true) {
            1f
        } else {
            0f
        }
    }

}