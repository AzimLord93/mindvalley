package com.mindvalley.android.util

import android.content.Context
import com.mindvalley.android.R
import java.net.UnknownHostException

object ErrorManager {

    class LocalDataAvailableException: Exception() {
        override val message = "Local Data Is Available"
    }

    fun getErrorMessage(context: Context, throwable: Throwable?): String {
        return when (throwable) {
            is UnknownHostException -> {
                context.getString(R.string.error_connection_message)
            }
            is LocalDataAvailableException -> {
                context.getString(R.string.error_connection_message_offline_available)
            }
            else -> {
                context.getString(R.string.error_default_message)
            }
        }
    }

}