package com.mindvalley.android.ui.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.mindvalley.android.R
import com.mindvalley.android.data.entities.Category
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.data.entities.Media
import com.mindvalley.android.databinding.ItemSectionCategoryBinding
import com.mindvalley.android.databinding.ItemSectionChannelBinding
import com.mindvalley.android.databinding.ItemSectionNewEpisodeBinding
import com.mindvalley.android.util.GridSpacingItemDecoration
import com.mindvalley.android.util.dp

class SectionAdapter(private val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var newEpisodes = emptyList<Media>()
    private var channels = emptyList<Channel>()
    private var categories = emptyList<Category>()

    // You can re-arrange section by changing the position of Section enum
    enum class Section {
        NEW_EPISODES,
        CHANNELS,
        CATEGORIES
    }

    fun addNewEpisodes(newEpisodes: List<Media>) {
        this.newEpisodes = newEpisodes
        notifyItemChanged(Section.NEW_EPISODES.ordinal)
    }

    fun addChannels(channels: List<Channel>) {
        this.channels = channels
        notifyItemChanged(Section.CHANNELS.ordinal)
    }

    fun addCategories(categories: List<Category>) {
        this.categories = categories
        notifyItemChanged(Section.CATEGORIES.ordinal)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (Section.values()[viewType]) {
            Section.NEW_EPISODES -> {
                val binding: ItemSectionNewEpisodeBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_section_new_episode,
                    parent,
                    false
                )
                return SectionNewEpisodeViewHolder(binding)
            }
            Section.CHANNELS -> {
                val binding: ItemSectionChannelBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_section_channel,
                    parent,
                    false
                )
                return SectionChannelViewHolder(binding)
            }
            Section.CATEGORIES -> {
                val binding: ItemSectionCategoryBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_section_category,
                    parent,
                    false
                )
                return SectionCategoryViewHolder(binding)
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount() = 3

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SectionNewEpisodeViewHolder -> {
                holder.bind(newEpisodes)
            }
            is SectionChannelViewHolder -> {
                holder.bind(channels)
            }
            is SectionCategoryViewHolder -> {
                holder.bind(categories)
            }
        }
    }

    inner class SectionNewEpisodeViewHolder(private val binding: ItemSectionNewEpisodeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(medias: List<Media>) {
            if (medias.isEmpty()) {
                binding.root.visibility = View.GONE
            } else {
                binding.root.visibility = View.VISIBLE
                val adapter = CourseAdapter(context)
                adapter.addMedias(medias)
                binding.rvMedia.adapter = adapter

                if (binding.rvMedia.itemDecorationCount == 0) {
                    ContextCompat.getDrawable(context, R.drawable.divider_media)?.let {
                        val itemDecorator = DividerItemDecoration(
                            binding.rvMedia.context,
                            DividerItemDecoration.HORIZONTAL
                        )
                        itemDecorator.setDrawable(it)
                        binding.rvMedia.addItemDecoration(itemDecorator)
                    }
                }
            }

        }
    }

    inner class SectionChannelViewHolder(private val binding: ItemSectionChannelBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(channels: List<Channel>) {
            if (channels.isEmpty()) {
                binding.root.visibility = View.GONE
            } else {
                binding.root.visibility = View.VISIBLE
                val adapter = ChannelAdapter(context, channels)
                binding.rvChannel.adapter = adapter
            }
        }
    }

    inner class SectionCategoryViewHolder(private val binding: ItemSectionCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(categories: List<Category>) {
            if (categories.isEmpty()) {
                binding.root.visibility = View.GONE
            } else {
                binding.root.visibility = View.VISIBLE
                val adapter = CategoryAdapter(context, categories)
                binding.rvCategory.adapter = adapter

                if (binding.rvCategory.itemDecorationCount == 0) {
                    val itemDecoration = GridSpacingItemDecoration(
                        context.resources.getInteger(R.integer.category_span_count),
                        15.dp,
                        false
                    )
                    binding.rvCategory.addItemDecoration(itemDecoration)
                }
            }
        }
    }

}