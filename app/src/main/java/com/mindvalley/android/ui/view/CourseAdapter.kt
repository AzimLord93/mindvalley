package com.mindvalley.android.ui.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mindvalley.android.R
import com.mindvalley.android.data.entities.Media
import com.mindvalley.android.databinding.ItemCourseBinding
import com.mindvalley.android.ui.viewmodel.MediaViewModel
import com.mindvalley.android.util.Constants

class CourseAdapter(private val context: Context) :
    RecyclerView.Adapter<CourseAdapter.CourseViewHolder>() {

    private var medias: List<Media> = emptyList()

    fun addMedias(medias: List<Media>?) {
        medias?.let {
            this.medias = it
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseViewHolder {
        val binding: ItemCourseBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_course,
            parent,
            false
        )
        return CourseViewHolder(binding)
    }

    override fun getItemCount() =
        if (medias.size > Constants.MAX_NUM_SECTION_ITEM) {
            Constants.MAX_NUM_SECTION_ITEM
        } else {
            medias.size
        }

    override fun onBindViewHolder(holder: CourseViewHolder, position: Int) {
        holder.bind(medias[position])
    }

    inner class CourseViewHolder(private val binding: ItemCourseBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(media: Media) {
            val viewModel = MediaViewModel(media)
            binding.viewModel = viewModel
        }
    }

}