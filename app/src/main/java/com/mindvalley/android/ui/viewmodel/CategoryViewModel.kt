package com.mindvalley.android.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mindvalley.android.data.entities.Category
import com.mindvalley.android.ui.base.BaseViewModel

class CategoryViewModel(val category: Category): BaseViewModel() {

    val name = MutableLiveData(category.name)

}