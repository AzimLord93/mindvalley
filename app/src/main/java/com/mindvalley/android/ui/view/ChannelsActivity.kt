package com.mindvalley.android.ui.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.util.Log
import android.view.animation.LinearInterpolator
import com.mindvalley.android.R
import com.mindvalley.android.databinding.ActivityChannelsBinding
import com.mindvalley.android.ui.base.BaseActivity
import com.mindvalley.android.ui.viewmodel.ChannelsViewModel
import com.mindvalley.android.util.ErrorManager
import com.mindvalley.android.util.dp
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChannelsActivity : BaseActivity<ActivityChannelsBinding, ChannelsViewModel>() {

    override val viewModel: ChannelsViewModel by viewModel()
    lateinit var adapter: SectionAdapter

    override fun getContentView() = R.layout.activity_channels

    override fun onCreate() {
        setupRecyclerView()
        setupSwipeRefresh()
        setupObservers()

        viewModel.getChannelDataFromServer()

        val config = getResources().getConfiguration()
        val (screenWidthPx, screenHeightPx) = config.screenWidthDp.dp to config.screenHeightDp.dp
        Log.e("Width in dp", config.screenWidthDp.toString())
        Log.e("Width in px", screenWidthPx.toString())
    }

    private fun setupRecyclerView() {
        adapter = SectionAdapter(this)
        binding.rvSection.adapter = adapter
    }

    private fun setupSwipeRefresh() {
        binding.vSwipeRefresh.setOnRefreshListener {
            viewModel.isShowRoot.value = false
            viewModel.getChannelDataFromServer()
            viewModel.resetMessage()
        }
    }

    private fun setupObservers() {
        viewModel.channelPage.observe(this) {
            it?.let {
                adapter.addNewEpisodes(it.newEpisodes)
                adapter.addChannels(it.channels)
                adapter.addCategories(it.categories)

                val throwable = viewModel.exception.value
                if (throwable != null) {
                    if (throwable is ErrorManager.LocalDataAvailableException) {
                        animateOfflineData()
                    }
                } else {
                    viewModel.isShowRoot.value = true
                }
            }
        }

        viewModel.exception.observe(this) {
            it?.let { throwable ->
                val message = ErrorManager.getErrorMessage(this, throwable)
                viewModel.message.value = message
                viewModel.isShowMessage.value = true
                viewModel.getChannelDataFromDatabase()
            }
        }
    }

    private fun animateOfflineData() {
        viewModel.isShowRoot.value = false
        binding.vMessage.animate().apply {
            interpolator = LinearInterpolator()
            duration = 500
            alpha(0f)
            startDelay = 1250
            setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    viewModel.isShowMessage.value = false
                    animateRootView()
                }
            })
            start()
        }
    }

    private fun animateRootView() {
        viewModel.isShowRoot.value = false
        binding.vRoot.animate().apply {
            interpolator = LinearInterpolator()
            duration = 500
            alpha(1f)
            setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    viewModel.isShowRoot.value = true
                }
            })
            start()
        }
    }
}