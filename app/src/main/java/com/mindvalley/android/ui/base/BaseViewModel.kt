package com.mindvalley.android.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import org.koin.core.component.KoinComponent

open class BaseViewModel : ViewModel(), KoinComponent {

    val isShowRoot = MutableLiveData(false)
    val isLoading = MutableLiveData(false)
    val isShowMessage = MutableLiveData(false)
    val message = MutableLiveData("")
    val exception = MutableLiveData<Throwable?>(null)

    fun resetMessage() {
        exception.postValue(null)
        isShowMessage.postValue(false)
        message.postValue("")
    }
}