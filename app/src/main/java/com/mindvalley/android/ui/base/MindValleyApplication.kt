package com.mindvalley.android.ui.base

import android.app.Application
import com.mindvalley.android.koin.mindValleyModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MindValleyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // Reference Android contexts
            androidContext(this@MindValleyApplication)
            // Load modules
            modules(
                mindValleyModule
            )
        }
    }

}