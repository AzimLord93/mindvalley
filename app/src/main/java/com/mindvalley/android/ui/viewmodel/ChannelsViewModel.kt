package com.mindvalley.android.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mindvalley.android.data.entities.ChannelPage
import com.mindvalley.android.data.repository.ChannelsRepository
import com.mindvalley.android.data.room.MindvalleyDatabase
import com.mindvalley.android.ui.base.BaseViewModel
import com.mindvalley.android.util.ErrorManager
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.UnknownHostException

class ChannelsViewModel(
    private val repository: ChannelsRepository,
    private val database: MindvalleyDatabase
) : BaseViewModel() {

    val channelPage = MutableLiveData<ChannelPage?>(null)

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        when (throwable) {
            is UnknownHostException -> {
                viewModelScope.launch(Dispatchers.IO) {
                    if (repository.isOfflineDataAvailable()) {
                        postError(ErrorManager.LocalDataAvailableException())
                    } else {
                        postError(throwable)
                    }
                }
            }
            else -> {
                postError(throwable)
            }
        }
    }

    private fun postError(throwable: Throwable) {
        exception.postValue(throwable)
        isLoading.postValue(false)
    }

    fun getChannelDataFromServer() {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val categoriesResponse =
                withContext(Dispatchers.Default) {
                    repository.getCategoriesRemote()
                }

            val channelsResponse =
                withContext(Dispatchers.Default) {
                    repository.getChannelsRemote()
                }

            val newEpisodesResponse =
                withContext(Dispatchers.Default) {
                    repository.getNewEpisodesRemote()
                }

            if (!categoriesResponse.isSuccessful && !channelsResponse.isSuccessful && !newEpisodesResponse.isSuccessful) {
                coroutineExceptionHandler.handleException(coroutineContext, Exception())
            } else {
                database.clearAllTables()
                repository.storeCategoriesLocally(categoriesResponse)
                repository.storeChannelsLocally(channelsResponse)
                repository.storeNewEpisodesLocally(newEpisodesResponse)

                getChannelDataFromDatabase()
            }
        }
    }

    fun getChannelDataFromDatabase() {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val categories =
                withContext(Dispatchers.Default) {
                    repository.getCategoriesLocal()
                }
            val channels =
                withContext(Dispatchers.Default) {
                    repository.getChannelsLocal()
                }
            val newEpisodes =
                withContext(Dispatchers.Default) {
                    repository.getNewEpisodesLocal()
                }

            channelPage.postValue(ChannelPage(categories, channels, newEpisodes))
            isLoading.postValue(false)
        }
    }

}