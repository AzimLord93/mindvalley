package com.mindvalley.android.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.ui.base.BaseViewModel

class ChannelViewModel(channel: Channel): BaseViewModel() {

    val name = MutableLiveData(channel.title)
    val episodeCount = MutableLiveData("")
    val iconUrl = MutableLiveData(channel.iconAsset?.url)

}