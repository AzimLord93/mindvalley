package com.mindvalley.android.ui.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.data.entities.Media
import com.mindvalley.android.ui.base.BaseViewModel
import kotlin.run

class MediaViewModel(media: Media): BaseViewModel() {
    val coverUrl = MutableLiveData(media.coverAsset?.url)
    val title = MutableLiveData(media.title)
    val channel = MutableLiveData("")
    val channelVisibility = MutableLiveData(View.VISIBLE)

    init {
        setChannel(media.channel)
    }

    private fun setChannel(channel: Channel?) {
        channel?.let {
            this.channel.value = channel.title
            this.channelVisibility.value = View.VISIBLE
        } ?: run {
            this.channelVisibility.value = View.GONE
        }
    }
}