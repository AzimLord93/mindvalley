package com.mindvalley.android.ui.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mindvalley.android.R
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.databinding.ItemChannelCourseBinding
import com.mindvalley.android.ui.viewmodel.ChannelViewModel

class ChannelAdapter(private val context: Context, private val channels: List<Channel>) :
    RecyclerView.Adapter<ChannelAdapter.ChannelCourseViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChannelAdapter.ChannelCourseViewHolder {
        val binding: ItemChannelCourseBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_channel_course,
            parent,
            false
        )
        return ChannelCourseViewHolder(binding)
    }

    override fun getItemCount() = channels.size

    override fun onBindViewHolder(holder: ChannelAdapter.ChannelCourseViewHolder, position: Int) {
        holder.bind(channels[position])
    }

    inner class ChannelCourseViewHolder(private val binding: ItemChannelCourseBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(channel: Channel) {
            if (channel.series.isNullOrEmpty()) {
                val viewModel = ChannelViewModel(channel)
                binding.viewModel = viewModel
                if (channel.latestMedia.isNullOrEmpty()) {
                    viewModel.episodeCount.value =
                        context.resources.getString(R.string.number_of_episode, 0)
                } else {
                    viewModel.episodeCount.value = context.resources.getString(
                        R.string.number_of_episode,
                        channel.latestMedia!!.size
                    )
                }

                val adapter = CourseAdapter(context)
                adapter.addMedias(channel.latestMedia)
                binding.rvMedia.adapter = adapter
            } else {
                val viewModel = ChannelViewModel(channel)
                binding.viewModel = viewModel
                if (channel.series.isNullOrEmpty()) {
                    viewModel.episodeCount.value =
                        context.resources.getString(R.string.number_of_series, 0)
                } else {
                    viewModel.episodeCount.value = context.resources.getString(
                        R.string.number_of_series,
                        channel.series!!.size
                    )
                }

                val adapter = SeriesAdapter(context)
                adapter.addMedias(channel.series)
                binding.rvMedia.adapter = adapter
            }

            if (binding.rvMedia.itemDecorationCount == 0) {
                ContextCompat.getDrawable(context, R.drawable.divider_media)?.let {
                    val itemDecorator = DividerItemDecoration(
                        binding.rvMedia.context,
                        DividerItemDecoration.HORIZONTAL
                    )
                    itemDecorator.setDrawable(it)
                    binding.rvMedia.addItemDecoration(itemDecorator)
                }
            }
        }
    }

}