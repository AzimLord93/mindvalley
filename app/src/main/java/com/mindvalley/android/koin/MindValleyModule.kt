package com.mindvalley.android.koin

import com.mindvalley.android.data.api.RetrofitBuilder
import com.mindvalley.android.ui.base.BaseViewModel
import com.mindvalley.android.ui.viewmodel.ChannelsViewModel
import com.mindvalley.android.data.repository.ChannelsRepository
import com.mindvalley.android.data.room.MindvalleyDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val mindValleyModule = module {
    // region Network

    single { RetrofitBuilder.getApiService(androidContext()) }

    // endregion
    // region Repositories

    singleOf(::ChannelsRepository)

    // endregion
    // region ViewModels

    viewModelOf(::BaseViewModel)
    viewModelOf(::ChannelsViewModel)

    // endregion
    // region Room

    single { MindvalleyDatabase.newInstance(androidContext()) }
    single { get<MindvalleyDatabase>().categoryDao() }
    single { get<MindvalleyDatabase>().mediaDao() }
    single { get<MindvalleyDatabase>().channelDao() }

    // endregion
}