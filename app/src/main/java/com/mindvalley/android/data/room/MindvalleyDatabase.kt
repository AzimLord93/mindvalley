package com.mindvalley.android.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mindvalley.android.data.entities.Category
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.data.entities.Media
import com.mindvalley.android.data.room.dao.CategoryDao
import com.mindvalley.android.data.room.dao.ChannelDao
import com.mindvalley.android.data.room.dao.MediaDao

@Database(entities = [Category::class, Media::class, Channel::class], version = 7)
@TypeConverters(value = [Converters::class])
abstract class MindvalleyDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "mindvalley_db"

        fun newInstance(context: Context): MindvalleyDatabase {
            return Room.databaseBuilder(
                context,
                MindvalleyDatabase::class.java,
                DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun categoryDao(): CategoryDao

    abstract fun mediaDao(): MediaDao

    abstract fun channelDao(): ChannelDao

}