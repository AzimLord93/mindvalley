package com.mindvalley.android.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mindvalley.android.data.entities.Category
import com.mindvalley.android.data.entities.Media

@Dao
interface MediaDao {

    @Insert()
    suspend fun insertAll(medias: List<Media>)

    @Query("SELECT * FROM media")
    suspend fun getAll(): List<Media>

}