package com.mindvalley.android.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mindvalley.android.data.entities.Category

@Dao
interface CategoryDao {

    @Insert()
    suspend fun insertAll(categories: List<Category>)

    @Query("SELECT * FROM category")
    suspend fun getAll(): List<Category>

}