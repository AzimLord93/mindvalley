package com.mindvalley.android.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(
    var name: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
