package com.mindvalley.android.data.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class Media(
    var type: String?,
    var title: String?,
    @Embedded(prefix = "coverAsset")
    var coverAsset: Asset?,
    @Embedded(prefix = "channel")
    var channel: Channel?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}
