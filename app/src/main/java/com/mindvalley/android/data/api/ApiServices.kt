package com.mindvalley.android.data.api

import com.mindvalley.android.data.api.response.BaseResponse
import com.mindvalley.android.data.api.response.CategoriesResponse
import com.mindvalley.android.data.api.response.ChannelsResponse
import com.mindvalley.android.data.api.response.NewEpisodesResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiServices {

    @GET("A0CgArX3")
    suspend fun getCategories(): Response<BaseResponse<CategoriesResponse>>

    @GET("Xt12uVhM")
    suspend fun getChannels(): Response<BaseResponse<ChannelsResponse>>

    @GET("z5AExTtw")
    suspend fun getNewEpisodes(): Response<BaseResponse<NewEpisodesResponse>>

}