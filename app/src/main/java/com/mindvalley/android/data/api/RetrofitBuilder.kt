package com.mindvalley.android.data.api

import android.content.Context
import com.google.gson.GsonBuilder
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import com.mindvalley.android.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private const val BASE_URL = "https://pastebin.com/raw/"

    fun getApiService(context: Context): ApiServices {
        return getRetrofit(context).create(ApiServices::class.java)
    }

    private fun getRetrofit(context: Context): Retrofit {
        val gson = GsonBuilder()
            .create()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(getOkhttpClient(context))
            .build()
    }

    private fun getOkhttpClient(context: Context): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(OkHttpProfilerInterceptor())
        }
        builder.addInterceptor(
            HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BASIC))
//        builder.addInterceptor(ConnectivityInterceptor(context))
//        builder.addInterceptor(TokenInterceptor())
        return builder.build()
    }
}