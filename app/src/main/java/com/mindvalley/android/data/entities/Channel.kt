package com.mindvalley.android.data.entities

import androidx.room.*

@Entity
data class Channel(
    var title: String?,
    var mediaCount: Int?,
    @Embedded(prefix = "iconAsset")
    var iconAsset: Asset?,
    @Embedded(prefix = "coverAsset")
    var coverAsset: Asset?,
    var series: ArrayList<Media>?,
    var latestMedia: ArrayList<Media>?
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var localId: Int = 0
}
