package com.mindvalley.android.data.api.response

import com.mindvalley.android.data.entities.Channel

data class ChannelsResponse(
    val channels: ArrayList<Channel>?
)