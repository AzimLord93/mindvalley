package com.mindvalley.android.data.api.response

import com.mindvalley.android.data.entities.Category

data class CategoriesResponse(
    val categories: ArrayList<Category>?
)