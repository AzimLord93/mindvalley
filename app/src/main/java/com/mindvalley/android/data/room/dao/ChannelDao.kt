package com.mindvalley.android.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mindvalley.android.data.entities.Channel

@Dao
interface ChannelDao {

    @Insert()
    suspend fun insertAll(channels: List<Channel>)

    @Query("SELECT * FROM channel")
    suspend fun getAll(): List<Channel>

}