package com.mindvalley.android.data.room

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mindvalley.android.data.entities.Media

class Converters {

    @TypeConverter
    fun fromJsonMedia(value: String?): ArrayList<Media>? {
        val listType = object :
            TypeToken<ArrayList<Media>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListMedia(list: ArrayList<Media>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }

}