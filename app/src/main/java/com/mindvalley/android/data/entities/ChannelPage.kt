package com.mindvalley.android.data.entities

data class ChannelPage(
    val categories: List<Category> = emptyList(),
    val channels: List<Channel> = emptyList(),
    val newEpisodes: List<Media> = emptyList()
)
