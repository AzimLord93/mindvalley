package com.mindvalley.android.data.api.response

import com.mindvalley.android.data.entities.Media

data class NewEpisodesResponse(
    val media: ArrayList<Media>?
)