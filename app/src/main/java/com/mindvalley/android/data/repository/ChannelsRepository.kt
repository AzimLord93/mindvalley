package com.mindvalley.android.data.repository

import android.util.Log
import com.mindvalley.android.data.api.ApiServices
import com.mindvalley.android.data.api.response.BaseResponse
import com.mindvalley.android.data.api.response.CategoriesResponse
import com.mindvalley.android.data.api.response.ChannelsResponse
import com.mindvalley.android.data.api.response.NewEpisodesResponse
import com.mindvalley.android.data.entities.Category
import com.mindvalley.android.data.entities.Channel
import com.mindvalley.android.data.entities.Media
import com.mindvalley.android.data.room.dao.CategoryDao
import com.mindvalley.android.data.room.dao.ChannelDao
import com.mindvalley.android.data.room.dao.MediaDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Response

class ChannelsRepository(
    private val apiServices: ApiServices,
    private val categoryDao: CategoryDao,
    private val mediaDao: MediaDao,
    private val channelDao: ChannelDao
) {

    // region Categories
    suspend fun getCategoriesRemote() = apiServices.getCategories()
    suspend fun getCategoriesLocal() = categoryDao.getAll()
    // endregion
    // region Channels
    suspend fun getChannelsRemote() = apiServices.getChannels()
    suspend fun getChannelsLocal() = channelDao.getAll()
    // endregion
    // region New Episodes
    suspend fun getNewEpisodesRemote() = apiServices.getNewEpisodes()
    suspend fun getNewEpisodesLocal() = mediaDao.getAll()
    // endregion

    suspend fun isOfflineDataAvailable(): Boolean {
        val localCategories = categoryDao.getAll()
        val localChannels = channelDao.getAll()
        val localNewEpisodes = mediaDao.getAll()

        return localCategories.isNotEmpty() && localChannels.isNotEmpty() && localNewEpisodes.isNotEmpty()
    }

    suspend fun storeCategoriesLocally(categoriesResponse: Response<BaseResponse<CategoriesResponse>>) {
        if (categoriesResponse.isSuccessful) {
            categoriesResponse.body()?.let { body ->
                body.getData()?.let { data ->
                    data.categories?.let {
                        categoryDao.insertAll(it.toList())
                    }
                }
            }
        }
    }

    suspend fun storeChannelsLocally(channelsResponse: Response<BaseResponse<ChannelsResponse>>) {
        if (channelsResponse.isSuccessful) {
            channelsResponse.body()?.let { body ->
                body.getData()?.let { data ->
                    data.channels?.let {
                        channelDao.insertAll(it.toList())
                    }
                }
            }
        }
    }

    suspend fun storeNewEpisodesLocally(newEpisodesResponse: Response<BaseResponse<NewEpisodesResponse>>) {
        if (newEpisodesResponse.isSuccessful) {
            newEpisodesResponse.body()?.let { body ->
                body.getData()?.let { data ->
                    data.media?.let {
                        mediaDao.insertAll(it.toList())
                    }
                }
            }
        }
    }

    fun getChannelPage(
        loading: (isLoading: Boolean) -> Unit,
        error: (exception: Exception) -> Unit,
        completed: (categories: List<Category>, channels: List<Channel>, newEpisodes: List<Media>) -> Unit
    ) {
        loading(true)
        try {
            CoroutineScope(Dispatchers.IO).launch {
                // If needed can create List extension to convert to ArrayList
                val categories = async {
                    categoryDao.getAll()
                }.await()

                val channels = async {
                    channelDao.getAll()
                }.await()

                val newEpisodes = async {
                    mediaDao.getAll()
                }.await()

                loading(false)
                completed(categories, channels, newEpisodes)
            }
        } catch (e: Exception) {
            Log.e("Exception", e.localizedMessage)
            loading(false)
            error(e)
        }
    }

}