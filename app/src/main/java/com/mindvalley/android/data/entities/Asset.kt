package com.mindvalley.android.data.entities

import com.google.gson.annotations.SerializedName

data class Asset(
    @SerializedName(value = "url", alternate = ["thumbnailUrl"])
    val url: String?
)
