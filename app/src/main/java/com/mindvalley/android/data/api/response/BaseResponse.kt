package com.mindvalley.android.data.api.response

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("data")
    private val responseData: T?
) {

    fun getData(): T? = responseData

}