# Mindvalley Test

This project is a demo app to fullfill Mindvalley test. [Click here](https://drive.google.com/file/d/1T9uCnLFhdlfscVPnNia2WWzuyfZvLYzO/view?usp=share_link) to view the requirements.

## Challenges
Unit test and UI test. These part are the worst part of my development skills. This is also my first time building UI test. I should practice it more in my daily development.

## Feature Improvements
Overall, I think the UI/UX is great.
I would add a button to view more feature at end of every horizontal scroll view with more that 6 items available. With this feature, user can know that there are more items to be view.